#include "Adafruit_Si7021.h"

// Si7021 I2C address: 64 (hard coded in Adafruit library)

class Si7021Readout {
    public:
        Si7021Readout();
        ~Si7021Readout();

        double read_temperature();
        double read_humidity();
        double calculate_dewpoint();

    private:
        Adafruit_Si7021 fSensor;
        double fNoValue;
};
